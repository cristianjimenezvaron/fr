---
title: Publications
name: Cristian Felipe Jiménez Varón
date: 2023-02-20T15:26:23-06:00
---

-  **Jimenez-Varon, C.F.**, Sun, Y., Shang, H. (2025). Forecasting density-valued functional panel data. *Australian & New Zealand Journal of Statistics*, accepted.([https://arxiv.org/pdf/2403.13340.pdf](https://arxiv.org/pdf/2403.13340.pdf))

- **Jimenez-Varon, C.F.**, Lee, H., Genton, MG., Sun, Y. (2025). Visualization and Assessment of Copula Structures. *Journal of the Computational and Graphical Statistics*.
([https://www.tandfonline.com/doi/full/10.1080/10618600.2024.2432978](https://www.tandfonline.com/doi/full/10.1080/10618600.2024.2432978))

-  **Jimenez-Varon, C.F.**, Sun, Y., Li, T-H. (2024). A semi-parametric estimation method for quantile coherence with an application to bivariate financial time series clustering. *Econometrics and Statistics*.
([https://www.sciencedirect.com/science/article/pii/S2452306224000716](https://www.sciencedirect.com/science/article/pii/S2452306224000716))

- **Jimenez-Varon, C.F.**, Harrou, F., Sun, Y. (2024). Pointwise Data Depth for Univariate and Multivariate Functional Outlier Detection. *Environmetrics*. ([ http://doi.org/10.1002/env.2851]( http://doi.org/10.1002/env.2851))

-  **Jimenez-Varon, C.F.**, Sun, Y., Shang, H. (2024).Forecasting high-dimensional functional time series: Application to sub-national age-specific mortality. *Journal of the Computational and Graphical Statistics*.
([https://www.tandfonline.com/doi/full/10.1080/10618600.2024.2319166](https://www.tandfonline.com/doi/full/10.1080/10618600.2024.2319166))


- **Projets collaboratifs**

- Orozco-Arias, S., Jaimes, P.A., Candamil, M.S., **Jimenez-Varon, C.F.,** Tabares-Soto, R., Isaza, G., Guyot, R.(2021) InpactorDB: A Classified Lineage-Level Plant LTR Retrotransposon Reference Library for Free-Alignment Methods Based on Machine Learning}.  *Genes*. ([https://www.mdpi.com/2073-4425/12/2/190](https://www.mdpi.com/2073-4425/12/2/190)).


- Orozco-Arias, S., Tobon-Orozco, N., Pina, J.S., **Jimenez-Varon, C.F.**, Tabares-Soto, R., Guyot, R  (2021). TIP finder: An HPC Software to Detect Transposable Element Insertion Polymorphisms in Large Genomic Datasets.. 
*Biology*.
([https://www.mdpi.com/822494](https://www.mdpi.com/822494)).

- Tabares-Soto R, Orozco-Arias S, Romero-Cano V, Segovia Bucheli V, Rodríguez-Sotelo JL., **Jimenez-Varon, C.F.** (2020). A comparative study of machine learning and deep learning algorithms to classify cancer types based on microarray gene expression data. *PeerJ Computer Science*. ([https://doi.org/10.7717/peerj-cs.270](https://doi.org/10.7717/peerj-cs.270)).

- **Jimenez-Varon, C.F.**, Tabares Soto, R. (2018). Dinámica no lineal del mercado del café en Colombia. *Revista Mutis*, ([https://doi.org/10.21789/22561498.1405](https://doi.org/10.21789/22561498.1405)).