---
title: Home
name: Cristian Felipe Jiménez Varón
date: 2023-02-20T15:26:23-06:00

categories: [one]
tags: [two,three,four]
interestslabel: "Domaines de recherche"
interests:
 [
  "Séries temporelles sur réseau",
  "Analyse spectrale des quantiles dans les séries temporelles",
  "Analyse non paramétrique des données fonctionnelles",
  "Prévision des séries temporelles fonctionnelles",
  "Détection des valeurs aberrantes dans les données fonctionnelles",
  "Visualisation des données",
  "Statistiques non paramétriques",
  "Analyse de regroupement",
  "Statistiques non paramétriques à haute dimension"
  ]
applicationslabel: Applications
applications:
 [
  "Environnement",
  "Économie",
  "Biostatistique",
  "Finance"
  ]

postdoctoralelabel: Expérience postdoctorale

postdoctorale:
[
  dddd:
    course: "Associé de recherche en statistique. [York Nest Node](https://nest-programme.ac.uk/)"
    university: "University of York, RU"
    years: "09/2024 - 09/2027"
    website: "https://www.york.ac.uk/maths/people/cristian-varon/"
  ]  
educationlabel: Éducation

education:
[
  aaa:
    course: Doctorat en statistique
    university: King Abdullah University of Science and Technology (KAUST)
    years: 08/2020 - 05/2024
    Thèse de doctorat:  Visualisation, caractérisation et prévision des séries temporelles multivariées et                         fonctionnelles (rédigé en anglais)
    Date de la soutenance: 22/05/2024
  bbb:
    course: Master en mathématiques appliquées
    university: Universidad Nacional de Colombia
    years: 08/2016 - 12/2017
  ccc:
    course: Licence en génie industriel
    university: Universidad Nacional de Colombia
    years: 08/2012 - 12/2017
  zzz:
    course: Licence en génie chimique
    university: Universidad Nacional de Colombia
    years: 01/2010 - 12/2015
    ]
---


Je suis chercheur en analyse des **séries temporelles** et des **données fonctionnelles**. Mes recherches se concentrent principalement sur le développement de méthodes statistiques pour modéliser différents types de données couramment rencontrées dans les applications environnementales, économiques et financières.
